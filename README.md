# Classy Deep Embedding

This repo contains a library (`Data.Classy`) for an enhanced version of classy deep embedding where some of the syntax and boilerplate burden is alleviated with advanced type system extensions.
Furthermore, it shows some example applications:

- Expr: an expression language
- ExprGADT: a more type-safe expression language using GADTs
- Sharing: detection of sharing in expressions (modelled after: O. Kiselyov, ‘Implementing Explicit and Finding Implicit Sharing in Embedded DSLs’, Electronic Proceedings in Theoretical Computer Science, vol. 66, pp. 210–225, Sep. 2011, doi: 10.4204/eptcs.66.11.)
- SharingGADT: a GADT version of the above
- Region: a region DSL (modelled after: Y. Sun, U. Dhandhania, and B. C. d. S.  Oliveira, ‘Compositional Embeddings of Domain-Specific Languages’, Proc. ACM Program. Lang., vol. 6, no. OOPSLA2, p. 34, 2022, doi: 10.1145/3563294.)

## Publications

- M. Lubbers, Orchestrating the Internet of Things with Task-Oriented Programming. in Radboud Dissertation Series, no. DIS-002. Nijmegen: Radboud University Press, 2023. doi: 10.54195/9789493296114.
- M. Lubbers, ‘Deep Embedding with Class’, in Trends in Functional Programming, W. Swierstra and N. Wu, Eds., Cham: Springer International Publishing, 2022, pp. 39–58. doi: 10.1007/978-3-031-21314-4_3.

## Author

Mart Lubbers (mart@cs.ru.nl)

## License

See [LICENSE](LICENSE)
