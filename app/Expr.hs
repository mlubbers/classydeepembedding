module Expr where

import Data.Maybe
import Data.Classy

main :: IO ()
main = do
    putStrLn "e0"
    putStrLn $ "text      : " ++ text e0
    putStrLn $ "eval      : " ++ show (eval e0)
    putStrLn $ "eval . opt: " ++ show (eval (opt e0))
    putStrLn $ "text . opt: " ++ text (opt e0)
    putStrLn "e1"
    putStrLn $ "text      : " ++ text e1
    putStrLn $ "eval      : " ++ show (eval e1)
    putStrLn $ "eval . opt: " ++ show (eval (opt e1))
    putStrLn $ "text . opt: " ++ text (opt e1)
    putStrLn "e2"
    putStrLn $ "text      : " ++ text e2
    putStrLn $ "eval      : " ++ show (eval e2)
    putStrLn $ "eval . opt: " ++ show (eval (opt e2))
    putStrLn $ "text . opt: " ++ text (opt e2)
    putStrLn "e2 (overloaded)"
    putStrLn $ "text      : " ++ text (e2' :: Expr '[Text])
    putStrLn $ "eval      : " ++ show (eval (e2' :: Expr '[Eval]))
    putStrLn $ "eval . opt: " ++ show (eval (opt (e2' :: Expr '[Eval,Opt])))
    putStrLn $ "text . opt: " ++ text (opt (e2' :: Expr '[Text,Opt]))
    putStrLn "e3 (overloaded)"
    putStrLn $ "text      : " ++ text (e3 :: Expr '[Text])
    putStrLn $ "eval      : " ++ show (eval (e3 :: Expr '[Eval]))
    putStrLn $ "eval . opt: " ++ show (eval (opt (e3 :: Expr '[Eval,Opt])))
    putStrLn $ "text . opt: " ++ text (opt (e3 :: Expr '[Text,Opt]))

e0 :: Expr '[Eval,Text,Opt]
e0 = Lit 42

e0' :: Typeable c => Expr c
e0' = Lit 42

e1 :: Expr '[Eval,Text,Opt]
e1 = e0 `Add` Lit 0 `Add` Lit 3

e1' :: Typeable c => Expr c
e1' = e0' `Add` Lit 0 `Add` Lit 3

-- This is what you need to do by hand if you want to construct the record
--e2' :: Expr '[Eval,Text]
--e2' = Ext
--    (Cons (Cons Nil) :: Record (Subt '[Eval, Text]) '[Eval, Text])
--    (Lit 43 `Subt` Lit 1)

-- But it can be automated using smart constructors and a createRecord class
e2 :: Expr '[Eval,Text,Opt]
e2 = e1 `subt` Lit 1

-- Or it can be overloaded (but this requires you to list the views)
e2' :: (UsingExt '[Subt] c) => Expr c
e2' = e1' `subt` Lit 1

-- Or it can be overloaded (but this requires you to list the views)
e3 :: (UsingExt '[Neg,Subt] c) => Expr c
e3 = neg (neg e2')

subt :: (UsingExt '[Subt] c) => Expr c -> Expr c -> Expr c
subt l r = Ext createRecord (l `Subt` r)

-- Main data ttype, similar to the original classy deep embedding
data Expr c
    = Lit Int
    | Add (Expr c) (Expr c)
    | forall x x0. (Typeable x, x0 c ~ x)
        => Ext (Record x c) x

class Eval v where
    eval :: v -> Int

-- The instance is simplified a lot, only the `In` constraint is now added
instance DependsOn '[Eval] s => Eval (Expr s) where
    eval (Lit i) = i
    eval (Add l r) = eval l + eval r
    eval (Ext r (e :: x)) = withDict (project r :: Dict (Eval x)) eval e

class Text v where
    text :: v -> String

instance DependsOn '[Text] s => Text (Expr s) where
    text (Lit i) = show i
    text (Add l r) = "(" ++ text l ++ "+" ++ text r ++ ")"
    text (Ext r (e :: x)) = withDict (project r :: Dict (Text x)) text e

data Subt c
    = Subt (Expr c) (Expr c)
    | Subtloop (Expr c)

instance DependsOn '[Eval] s => Eval (Subt s) where
    eval (Subt l r) = eval l - eval r
    eval (Subtloop e) = eval e
instance DependsOn '[Text] s => Text (Subt s) where
    text (Subt l r) = "(" ++ text l ++ "-" ++ text r ++ ")"
    text (Subtloop e) = text e

class Opt v where
    opt :: v -> v

instance DependsOn '[Opt] s => Opt (Expr s) where
    opt (Lit i) = Lit i
    opt (Add l r) = case (opt l, opt r) of
        (Lit 0, r') -> r'
        (l', Lit 0) -> l'
        (l', r') -> Add l' r'
    opt (Ext r (e :: x)) = Ext r (withDict (project r :: Dict (Opt x)) opt e)
instance DependsOn '[Opt] s => Opt (Subt s) where
    opt (Subtloop e) = Subtloop e
    opt (Subt l r) = case (opt l, opt r) of
        (l', Lit 0) -> Subtloop l'
        (l', r') -> Subt l' r'

data Neg c
    = Neg (Expr c)
    | Negloop (Expr c)

neg :: (UsingExt '[Neg] c) => Expr c -> Expr c
neg l = Ext createRecord (Neg l)

instance DependsOn '[Eval] s => Eval (Neg s) where
    eval (Neg x) = negate (eval x)
    eval (Negloop x) = eval x
instance DependsOn '[Text] s => Text (Neg s) where
    text (Neg x) = "(!" ++ text x ++ ")"
    text (Negloop x) = text x

instance DependsOn '[Opt] s => Opt (Neg s) where
    opt (Negloop e) = Negloop (opt e)
    opt (Neg e) = case opt e of
        (Ext _ (mExt -> Just (Neg x'))) -> Negloop x'
        e' -> Neg e'

class LoopOpt v where
    fromLoop :: (x c ~ v) => v -> Maybe (Expr c)
    loopOpt :: v -> v

instance DependsOn '[LoopOpt] c => LoopOpt (Expr c) where
    fromLoop _ = Nothing

    loopOpt (Add l r) = Add (loopOpt l) (loopOpt r)
    loopOpt (Lit i) = Lit i
    loopOpt (Ext r (e :: x)) = withDict (project r :: Dict (LoopOpt x)) $
        let e' = loopOpt e in fromMaybe (Ext r e') (fromLoop e')
instance DependsOn '[LoopOpt] c => LoopOpt (Subt c) where
    fromLoop (Subtloop e) = Just e
    fromLoop _ = Nothing

    loopOpt (Subt l r) = Subt (loopOpt l) (loopOpt r)
    loopOpt (Subtloop e) = Subtloop (loopOpt e)
instance DependsOn '[LoopOpt] c => LoopOpt (Neg c) where
    fromLoop (Negloop e) = Just e
    fromLoop _ = Nothing

    loopOpt (Neg t) = Neg (loopOpt t)
    loopOpt (Negloop e) = Negloop (loopOpt e)

-- Push negations down
class ProNeg v where
    proNeg :: Bool -> v -> v

instance DependsOn '[ProNeg] c => ProNeg (Expr c) where
    -- negation of literal
    proNeg n (Lit i) = Lit (if n then negate i else i)
    -- negate (x+y) = negate x + negate y
    proNeg n (Add l r) = Add (proNeg n l) (proNeg n r)
    proNeg n (Ext r (e :: x)) = withDict (project r :: Dict (ProNeg x)) (Ext r (proNeg n e))

instance DependsOn '[ProNeg] c => ProNeg (Subt c) where
    -- negate (x-y) = negate x - negate y
    proNeg n (Subt l r) = Subt (proNeg n l) (proNeg n r)
    proNeg n (Subtloop e) = Subtloop (proNeg n e)

instance DependsOn '[ProNeg] c => ProNeg (Neg c) where
    proNeg n (Neg l) = Negloop (proNeg (not n) l)
    proNeg n (Negloop e) = Negloop (proNeg n e)
