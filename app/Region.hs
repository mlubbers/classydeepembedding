module Region where

-- Modelled after Y. Sun, U. Dhandhania, and B. C. d. S. Oliveira, ‘Compositional Embeddings of Domain-Specific Languages’, Proc. ACM Program. Lang., vol. 6, no. OOPSLA2, p. 34, 2022, doi: 10.1145/3563294.

import Data.Classy

data Vector = Vector { x :: Double, y :: Double }
  deriving Show

-- Main data type
data RegionHudak c
    = Circle Double
    | Outside (RegionHudak c)
    | Union (RegionHudak c) (RegionHudak c)
    | Intersect (RegionHudak c) (RegionHudak c)
    | Translate Vector (RegionHudak c)
    | forall x. Typeable x => Ext (Record x c) x

-- Extension
data RegionHofer c
    = Univ
    | Empty
    | Scale Vector (RegionHudak c)

-- This trick is only required for constructors with no children
univ :: (UsingExt '[RegionHofer] c) => RegionHudak c
univ = let res = Ext dict Univ; dict = cast res; in res
  where cast :: UsingExt '[RegionHofer] c => RegionHudak c -> Record (RegionHofer c) c
        cast _ = createRecord

-- This trick is only required for constructors with no children
empty :: (UsingExt '[RegionHofer] c) => RegionHudak c
empty = let res = Ext dict Empty; dict = cast res; in res
  where cast :: UsingExt '[RegionHofer] c => RegionHudak c -> Record (RegionHofer c) c
        cast _ = createRecord

scale :: (UsingExt '[RegionHofer] c) => Vector -> RegionHudak c -> RegionHudak c
scale f = Ext createRecord . Scale f

-- Size interpretation
class Size v where
    size :: v -> Int

-- Instances
instance DependsOn '[Size] s => Size (RegionHudak s) where
    size (Circle _) = 1
    size (Outside a) = size a + 1
    size (Union a b) = size a + size b + 1
    size (Intersect a b) = size a + size b + 1
    size (Translate _ a) = size a + 1
    size (Ext r (e :: x)) = withDict (project r :: Dict (Size x)) size e

instance DependsOn '[Size] s => Size (RegionHofer s) where
    size (Scale _ a) = size a + 1
    size Univ = 1
    size Empty = 1

---- Text interpretation
class Text v where
    text :: v -> String

instance DependsOn '[Size,Text] s => Text (RegionHudak s) where
    text (Circle r) = "a circular region of radius " ++ show r
    text (Outside a) = "outside a region of size " ++ show (size a)
    text s@(Union _ _) = "the union of two regions of size " ++ show (size s) ++ " in total"
    text s@(Intersect _ _) = "the intersection of two regions of size " ++ show (size s) ++ " in total"
    text s@(Translate _ _) = "a translated region of size " ++ show (size s)
    text (Ext r (e :: x)) = withDict (project r :: Dict (Text x)) text e

instance DependsOn '[Size, Text] s => Text (RegionHofer s) where
    text s@(Scale _ _) = "a scaled region of size " ++ show (size s)
    text Univ = "a universal region"
    text Empty = "an empty region"

-- Mutual recursion
class IsUniv v where
    isUniv :: v -> Bool
class IsEmpty v where
    isEmpty :: v -> Bool

instance DependsOn '[IsUniv, IsEmpty] s => IsUniv (RegionHudak s) where
    isUniv (Outside a) = isEmpty a
    isUniv (Union a b) = isUniv a || isUniv b
    isUniv (Intersect a b) = isUniv a && isUniv b
    isUniv (Translate _ a) = isUniv a
    isUniv (Ext r (e :: x)) = withDict (project r :: Dict (IsUniv x)) isUniv e
    isUniv _ = False

instance DependsOn '[IsUniv, IsEmpty] s => IsUniv (RegionHofer s) where
    isUniv Univ = True
    isUniv (Scale _ a) = isUniv a
    isUniv _ = False

instance DependsOn '[IsUniv, IsEmpty] s => IsEmpty (RegionHudak s) where
    isEmpty (Outside a) = isUniv a
    isEmpty (Union a b) = isEmpty a && isEmpty b
    isEmpty (Intersect a b) = isEmpty a || isEmpty b
    isEmpty (Translate _ a) = isEmpty a
    isEmpty (Ext r (e :: x)) = withDict (project r :: Dict (IsEmpty x)) isEmpty e
    isEmpty _ = False

instance DependsOn '[IsUniv, IsEmpty] s => IsEmpty (RegionHofer s) where
    isEmpty (Scale _ a) = isEmpty a
    isEmpty Empty = True
    isEmpty _ = False

main :: IO ()
main = do
    print (size (empty :: RegionHudak '[Size]))
    print (size (univ :: RegionHudak '[Size]))
    print (size (Circle 1.0 :: RegionHudak '[Size]))
    print (size reg2)
    print (text (empty :: RegionHudak '[Size,Text]))
    print (text (univ :: RegionHudak '[Size,Text]))
    print (text (Circle 1.0 :: RegionHudak '[Size,Text]))
    print (text reg2)
    print (isEmpty (empty :: RegionHudak '[IsUniv,IsEmpty]))
    print (isUniv (empty :: RegionHudak '[IsUniv,IsEmpty]))
    print (isEmpty (univ :: RegionHudak '[IsUniv,IsEmpty]))
    print (isUniv (univ :: RegionHudak '[IsUniv,IsEmpty]))
    print (isEmpty (scale one empty :: RegionHudak '[IsUniv,IsEmpty]))
    print (isUniv (scale one empty :: RegionHudak '[IsUniv,IsEmpty]))
    print (isEmpty (scale one univ :: RegionHudak '[IsUniv,IsEmpty]))
    print (isUniv (scale one univ :: RegionHudak '[IsUniv,IsEmpty]))
    putStrLn $ "Size:  " ++ show (size reg')
    putStrLn $ "Size:  " ++ show (size (reg :: RegionHudak '[Size]))
    putStrLn $ "Text:  " ++ text reg'
    putStrLn $ "Text:  " ++ text (reg :: RegionHudak '[Size,Text])
    putStrLn $ "Univ:  " ++ show (isUniv reg')
    putStrLn $ "Univ:  " ++ show (isUniv (reg :: RegionHudak '[IsUniv,IsEmpty]))
    putStrLn $ "Empty: " ++ show (isEmpty reg')
    putStrLn $ "Empty: " ++ show (isEmpty (reg :: RegionHudak '[IsUniv,IsEmpty]))
    putStrLn (text (circles :: RegionHudak '[Size,Text]))
    putStrLn "\n= Examples from paper website =\n"
    main2

reg2 :: RegionHudak '[Size,Text]
reg2 = scale one (Circle 0.0)

-- Specific expressions require only the list of required interpretations
reg' :: RegionHudak '[IsUniv, IsEmpty, Text, Size]
reg' = reg

-- Overloaded expressions require only a listing of all used extensions
reg :: (UsingExt '[RegionHofer] c) => RegionHudak c
reg = scale (Vector 5 5) (Outside empty `Union` Circle 1)

one :: Vector
one = Vector 1 1

class Contains v where
    contains :: v -> Vector -> Bool

instance DependsOn '[Contains] s => Contains (RegionHudak s) where
    contains (Circle r) p = x p ** 2 + y p ** 2 <= r ** 2
    contains (Outside a) p = not (a `contains` p)
    contains (Union a b) p = a `contains` p || b `contains` p
    contains (Intersect a b) p = a `contains` p && b `contains` p
    contains (Translate (Vector x' y') a) p = a `contains` Vector (x p - x') (y p - y')
    contains (Ext r (e :: x)) p = withDict (project r :: Dict (Contains x)) contains e p

instance DependsOn '[Contains] s => Contains (RegionHofer s) where
    contains Univ _ = True
    contains Empty _ = False
    contains (Scale f a) p = a `contains` Vector (x p / x f) (y p / y f) 

circles :: Typeable c => RegionHudak c
circles = go 20 (2 ** 18)
  where go :: Typeable c => Int -> Double -> RegionHudak c
        go 0 _ = Outside (Outside (Circle 1.0))
        go n offset = let shared = go (n-1) (offset/2.0)
                      in Union (Translate (Vector (-offset) 0.0) shared)
                               (Translate (Vector offset    0.0) shared)

class Simplify v where
    simplify :: v -> v
-- Because we conjure terms of type RegionHofer, we need to have this
-- CreateRecord instance in scope.
instance (UsingExt '[RegionHofer] s, DependsOn '[IsUniv,IsEmpty] s) => Simplify (RegionHudak s) where
    simplify e
        | isUniv e = univ
        | isEmpty e = empty
        | otherwise = e
instance (DependsOn '[IsEmpty, IsUniv] s) => Simplify (RegionHofer s) where
    simplify e
        | isUniv e = Univ
        | isEmpty e = Empty
        | otherwise = e

-- Remove nested Outside's (and nested Ext's for fun)
class Eliminate v where
    eliminate :: v -> v
instance (DependsOn '[Eliminate] s) => Eliminate (RegionHudak s) where
    eliminate (Outside (Outside a)) = a
    eliminate (Outside a) = Outside (eliminate a)
    eliminate a@(Circle _) = a
    eliminate (Union a b) = Union (eliminate a) (eliminate b)
    eliminate (Intersect a b) = Intersect (eliminate a) (eliminate b)
    eliminate (Translate v a) = Translate v (eliminate a)
    eliminate (Ext r (e :: x)) = case fromDynamic (toDyn e) of
        Just (Ext r' e') -> Ext r' e'
        _ -> Ext r (withDict (project r :: Dict (Eliminate x)) eliminate e)

circles' :: RegionHudak '[Size,Eliminate]
circles' = circles

main2 :: IO ()
main2 = do
    putStrLn $ "(1) the example of sharing has a size of " ++ show (size circles') ++ " before transformation.";
    putStrLn $ "(2) the example of sharing has a size of " ++ show (size $ eliminate circles') ++ " after transformation.";
    putStrLn $ "(3) " ++ text universal
        ++ (if simplify universal `contains` Vector {x=0.0, y=0.0}
            then " contains " else " does not contain")
        ++ "the origin and can be simplified to " ++ text (simplify universal) ++ "."

annulus,ellipse,universal :: RegionHudak '[Text,Size,Contains,IsUniv,IsEmpty,Simplify]
annulus = Intersect (Outside (Circle 4.0)) (Circle 8.0)
ellipse = scale (Vector {x=4.0, y = 8.0 }) (Circle 1.0)
universal = Union (Outside empty) (Circle 1.0)
