module Main where

import qualified Expr
import qualified ExprGADT
import qualified Region
import qualified Sharing
import qualified SharingGADT

main :: IO ()
main = do
    putStrLn "= Expressions ="
    Expr.main
    putStrLn "= Expressions (GADT) ="
    ExprGADT.main
    putStrLn "= Regions ="
    Region.main
    putStrLn "= Sharing ="
    Sharing.main
    putStrLn "= SharingGADTs ="
    SharingGADT.main
