{-# LANGUAGE CPP, StandaloneDeriving #-}
module SharingGADT where

-- Modelled after: O. Kiselyov, ‘Implementing Explicit and Finding Implicit Sharing in Embedded DSLs’, Electronic Proceedings in Theoretical Computer Science, vol. 66, pp. 210–225, Sep. 2011, doi: 10.4204/eptcs.66.11.

import Prelude hiding (and,or)
import Data.Classy
import Data.Maybe
import Data.Bits
#if MIN_VERSION_base(4,16,0)
    hiding (And)
#endif
import Control.Monad.Reader
import Control.Monad.State
import qualified Data.Map as DM

main :: IO ()
main = do
    putStrLn $ "expa: " ++ text expa
    putStrLn $ "expb: " ++ text expb
    putStrLn $ "expmul4: " ++ text (expmul4 :: Exp '[Text] Int)
    putStrLn $ "sklansky: " ++ show (map text (sklansky Add (map (Variable . show) [1 :: Int ..4]) :: [Exp '[Text] Int]))
    putStrLn $ "testval4: " ++ show testval4
    putStrLn $ "n expmul4: " ++ show (runN (expmul4 :: Exp '[N] Int))
    putStrLn $ "n expmul8: " ++ show (runN (expmul8 :: Exp '[N] Int))
    putStrLn $ "testSklansky 4: " ++ show (testSklansky 4)
    putStrLn $ "size expmul4: " ++ show (size (expmul4 :: Exp '[Size] Int))
    putStrLn $ "size expmul4': " ++ show (size (expmul4' :: Exp '[Size] Int))
    putStrLn $ "valmul4: " ++ show (renv (expmul4 :: Exp '[REnv] Int) [("i1", 5)])
    putStrLn $ "valmul4': " ++ show (renv (expmul4' :: Exp '[REnv] Int) [("i1", 5)])
    putStrLn $ "valmul4'': " ++ show (renv (expmul4'' :: Exp '[REnv] Int) [("i1", 5)])
    putStrLn $ "show expmul4: " ++ s (expmul4 :: Exp '[S] Int) 0
    putStrLn $ "show expmul4: " ++ s (expmul4 :: Exp '[S] Int) 0
    putStrLn $ "size expmul4: " ++ show (size (expmul4 :: Exp '[Size] Int))
    putStrLn $ "size expmul4': " ++ show (size (expmul4'' :: Exp '[Size] Int))
    putStrLn $ "size mul 10 i: " ++ show (size (mul 12 (Variable "i") :: Exp '[Size] Int))
    putStrLn $ "size mul' 10 i: " ++ show (size (mul' 12 (Variable "i") :: Exp '[Size] Int))
    putStrLn "== without sharing =="
    putStrLn $ "n=19 (mul n (variable \"i\")): " ++ show (runN (mul (2 `shiftL` 19) (Variable "i") :: Exp '[N] Int))
    putStrLn $ "n=20 (mul n (variable \"i\")): " ++ show (runN (mul (2 `shiftL` 20) (Variable "i") :: Exp '[N] Int))
    putStrLn $ "n=21 (mul n (variable \"i\")): " ++ show (runN (mul (2 `shiftL` 21) (Variable "i") :: Exp '[N] Int))
    putStrLn "== with sharing =="
    putStrLn $ "n=19 (mul' n (variable \"i\")): " ++ show (runN (mul' (2 `shiftL` 19) (Variable "i") :: Exp '[N] Int))
    putStrLn $ "n=20 (mul' n (variable \"i\")): " ++ show (runN (mul' (2 `shiftL` 20) (Variable "i") :: Exp '[N] Int))
    putStrLn $ "n=21 (mul' n (variable \"i\")): " ++ show (runN (mul' (2 `shiftL` 21) (Variable "i") :: Exp '[N] Int))
    putStrLn $ "n=31 (mul' n (variable \"i\")): " ++ show (runN (mul' (2 `shiftL` 31) (Variable "i") :: Exp '[N] Int))
    putStrLn "== Some boolean expressions =="
    putStrLn $ "text: " ++ text eb
    putStrLn $ "size: " ++ show (size eb)
    putStrLn $ "S: " ++ show (s eb 0)
    putStrLn $ "REnv: " ++ show (renv eb [("a", 42)])
    putStrLn $ "N: " ++ show (runN eb)

eb :: Exp '[Text,S,REnv,N,Size] Bool
eb = cond
        (Constant 42 `eq` Variable "a")
        (litB True `and` litB False)
        ((Constant 4 `subt` Constant 1) `eq` neg (Constant 5))

data Exp c a where
    Constant :: Int -> Exp c Int
    Variable :: String -> Exp c Int
    Add :: Exp c Int -> Exp c Int -> Exp c Int
    Ext :: Record x c -> x a -> Exp c a

expa,expb :: Exp '[Text] Int
expa = Add (Constant 10) (Variable "i1")
expb = Add expa (Variable "i2")

mul :: Int -> Exp c Int -> Exp c Int
mul 0 _ = Constant 0
mul 1 x = x
mul n x | even n = mul (n `div` 2) (Add x x)
mul n x = Add x (mul (n-1) x)

expmul4,expmul8 :: Exp c Int
expmul4 = mul 4 (Variable "i1")
expmul8 = mul 8 (Variable "i1")

expmul4' :: Exp c Int
expmul4' = let x = Variable "i1"
          in let y = Add x x in Add y y

class Text v where
    text :: v a -> String

instance DependsOn '[Text] s => Text (Exp s) where
    text (Constant i) = show i
    text (Add l r) = "(" ++ text l ++ "+" ++ text r ++ ")"
    text (Variable v) = v
    text (Ext r (e :: x a)) = withDict (project r :: Dict (Text x)) text e

sklansky :: (a -> a -> a) -> [a] -> [a]
sklansky _ [] = []
sklansky _ [x] = [x]
sklansky f xs = left' ++ [f (last left') r | r <- right']
  where (left, right) = splitAt (length xs `div` 2) xs
        left' = sklansky f left
        right' = sklansky f right

class REnv v where
    renv :: v a -> [(String, Int)] -> a

instance DependsOn '[REnv] s => REnv (Exp s) where
    renv (Constant x) = pure x
    renv (Variable x) = asks (fromMaybe (error $ "no var: " ++ x) . lookup x)
    renv (Add l r) = (+) <$> renv l <*> renv r
    renv (Ext r (e :: x a)) = withDict (project r :: Dict (REnv x)) renv e

testval4 :: Int
testval4 = renv (expmul4 :: Exp '[REnv] Int) [("i1",5)]

type NodeId = Int
data Node
    = NConst Int
    | NVar String
    | NAdd NodeId NodeId
    | forall x.(Eq x, Ord x, Show x) => NExt x
instance Eq Node where l == r = show l == show r
instance Ord Node where compare l r = compare (show l) (show r)
deriving instance Show Node

data BiMap a = BM {nextIdx :: Int, mapto :: DM.Map a Int, mapfro :: DM.Map Int a} deriving Show
newtype DAG = DAG (BiMap Node) deriving Show

lookupKey :: Ord a => a -> BiMap a -> Maybe Int
lookupKey a = DM.lookup a . mapto

lookupVal :: Int -> BiMap a -> Maybe a
lookupVal a = DM.lookup a . mapfro

empty :: BiMap a
empty = BM 0 DM.empty DM.empty

insert :: Ord a => a -> BiMap a -> (Int, BiMap a)
insert e (BM i mt mf) = (i, BM (i+1) (DM.insert e i mt) (DM.insert i e mf))

class N v where
    nf :: v a -> State DAG NodeId

runN :: N v => v a -> (NodeId, DAG)
runN v = runState (nf v) (DAG empty)

instance DependsOn '[N] s => N (Exp s) where
    nf (Constant x) = hashcons (NConst x)
    nf (Variable x) = hashcons (NVar x)
    nf (Add l r) = NAdd <$> nf l <*> nf r >>= hashcons
    nf (Ext r (e :: x a)) = withDict (project r :: Dict (N x)) nf e

hashcons :: Node -> State DAG NodeId
hashcons e = do
    DAG m <- get
    case lookupKey e m of
        Nothing -> let (k, m') = insert e m
                   in  put (DAG m') >> pure k
        Just k -> pure k

testSklansky :: Int -> ([NodeId], DAG)
testSklansky n = runState sk (DAG empty)
  where sk = mapM nf (sklansky Add xs :: [Exp '[N] Int])
        xs = map (Variable . show) [1..n]

class Size v where
    size :: v a -> Int
instance DependsOn '[Size] s => Size (Exp s) where
    size (Constant _) = 1
    size (Variable _) = 1
    size (Add x y) = size x + size y + 1
    size (Ext r (e :: x a)) = withDict (project r :: Dict (Size x)) size e

data Let c b = forall a. Let (Exp c a) (Exp c a -> Exp c b)

let_ :: UsingExt '[Let] c => Exp c a -> (Exp c a -> Exp c b) -> Exp c b
let_ d b = Ext createRecord (Let d b)

instance DependsOn '[REnv] s => REnv (Let s) where
    renv (Let x f) = renv (f x)

instance (UsingExt '[VarIdTag] s, DependsOn '[Size] s) => Size (Let s) where
    -- Variables are always integers so we need a different tag
    size (Let x f) = size x + size (f (Ext (createRecord :: Record (VarIdTag s) s) (VarIdTag "")))
instance Size (VarIdTag c) where
    size _ = 1

expmul4'' :: UsingExt '[Let] c => Exp c Int
expmul4'' =
    let_ (Variable "i1") (\x->
    let_ (Add x x) (\y->
    Add y y))

class S v where
    s :: v a -> Int -> String

instance DependsOn '[S] s => S (Exp s) where
    s (Constant i) = pure (show i)
    s (Variable x) = pure x
    s (Add l r) = (\e1 e2->"(" ++ e1 ++ "+" ++ e2 ++ ")") <$> s l <*> s r
    s (Ext r (e :: x a)) = withDict (project r :: Dict (S x)) s e

instance (UsingExt '[VarIdTag] s, DependsOn '[S] s) => S (Let s) where
    s (Let def x) = do
        ni <- asks (("v"++) . show)
        pdef <- s def
        -- Variables are always integers so we need a different tag
        pbody <- local (+1) (s (x (Ext (createRecord :: Record (VarIdTag s) s) (VarIdTag ni))))
        pure ("let " ++ show ni ++ " = " ++ pdef ++ " in " ++ pbody)
newtype VarIdTag c a where
    VarIdTag :: String -> VarIdTag c a
instance S (VarIdTag s) where
    s (VarIdTag t) = pure t

instance (UsingExt '[NodeIdTag] s, DependsOn '[N] s) => N (Let s) where
    nf (Let e f) = nf e >>= nf . f . Ext (createRecord :: Record (NodeIdTag s) s) . NodeIdTag

-- Auxilliary type so that we can inject NodeId's
newtype NodeIdTag c a where
    NodeIdTag :: NodeId -> NodeIdTag c a
instance N (NodeIdTag c) where
    nf (NodeIdTag i) = pure i

mul' :: UsingExt '[Let] c => Int -> Exp c Int -> Exp c Int
mul' 0 _ = Constant 0
mul' 1 x = x
mul' n x
    | even n    = let_ x (\x'->mul' (n `div` 2) (Add x' x'))
    | otherwise = let_ x (\x'->Add x' (mul' (n-1) x'))

-- Add subtraction
data Subt c a where
    Subt :: Exp c Int -> Exp c Int -> Subt c Int
subt :: UsingExt '[Subt] c => Exp c Int -> Exp c Int -> Exp c Int
subt l r = Ext createRecord (Subt l r)
data NSubt = NSubt NodeId NodeId deriving (Eq, Ord, Show)
nSubt :: NodeId -> NodeId -> Node
nSubt l r = NExt (NSubt l r)
instance DependsOn '[Text] s => Text (Subt s) where
    text (Subt l r) = "(" ++ text l ++ "-" ++ text r ++ ")"
instance DependsOn '[REnv] s => REnv (Subt s) where
    renv (Subt l r) = (-) <$> renv l <*> renv r
instance DependsOn '[N] s => N (Subt s) where
    nf (Subt l r) = nSubt <$> nf l <*> nf r >>= hashcons
instance DependsOn '[Size] s => Size (Subt s) where
    size (Subt l r) = size l + size r + 1
instance DependsOn '[S] s => S (Subt s) where
    s (Subt l r) = (\e1 e2->"(" ++ e1 ++ "-" ++ e2 ++ ")") <$> s l <*> s r

-- Add Negation
data Neg c a where
    Neg :: Exp c Int -> Neg c Int
neg :: UsingExt '[Neg] c => Exp c Int -> Exp c Int
neg r = Ext createRecord (Neg r)
newtype NNeg = NNeg NodeId deriving (Eq, Ord, Show)
nNeg :: NodeId -> Node
nNeg l = NExt (NNeg l)
instance DependsOn '[Text] s => Text (Neg s) where
    text (Neg l) = "(!" ++ text l ++ ")"
instance DependsOn '[REnv] s => REnv (Neg s) where
    renv (Neg l) = negate <$> renv l
instance DependsOn '[N] s => N (Neg s) where
    nf (Neg l) = nf l >>= hashcons . nNeg
instance DependsOn '[Size] s => Size (Neg s) where
    size (Neg l) = size l + 1
instance DependsOn '[S] s => S (Neg s) where
    s (Neg l) = (\e1->"(!" ++ e1 ++ ")") <$> s l

-- Add booleans
data Boolean c a where
    LitB :: Bool -> Boolean c Bool
    And :: Exp c Bool -> Exp c Bool -> Boolean c Bool
    Or :: Exp c Bool -> Exp c Bool -> Boolean c Bool
    Cond :: Exp c Bool -> Exp c a -> Exp c a -> Boolean c a
    Eq :: Eq a => Exp c a -> Exp c a -> Boolean c Bool

litB :: UsingExt '[Boolean] c => Bool -> Exp c Bool
litB b = let res = Ext dict (LitB b); dict = cast res; in res
  where cast :: UsingExt '[Boolean] c => Exp c Bool-> Record (Boolean c) c
        cast _ = createRecord

and,or :: UsingExt '[Boolean] c => Exp c Bool -> Exp c Bool -> Exp c Bool
and l r = Ext createRecord (And l r)
or l r = Ext createRecord (Or l r)

cond :: UsingExt '[Boolean] c => Exp c Bool -> Exp c a -> Exp c a -> Exp c a
cond i t e = Ext createRecord (Cond i t e)

eq :: (Eq a, UsingExt '[Boolean] c) => Exp c a -> Exp c a -> Exp c Bool
eq l r = Ext createRecord (Eq l r)

data NBoolean = NLitB Bool | NAnd NodeId NodeId | NOr NodeId NodeId | NCond NodeId NodeId NodeId | NEq NodeId NodeId
  deriving (Eq, Ord, Show)
nLitB :: Bool -> Node
nLitB b = NExt (NLitB b)

nAnd,nOr,nEq :: NodeId -> NodeId -> Node
nAnd l r = NExt (NAnd l r)
nOr l r = NExt (NOr l r)
nEq l r = NExt (NEq l r)

nCond :: NodeId -> NodeId -> NodeId -> Node
nCond i t e = NExt (NCond i t e)

instance DependsOn '[Text] s => Text (Boolean s) where
    text (LitB b) = show b
    text (And l r) = "(" ++ text l ++ "&&" ++ text r ++ ")"
    text (Or l r) = "(" ++ text l ++ "||" ++ text r ++ ")"
    text (Cond i t e) = "if " ++ text i ++ " then " ++ text t ++ " else " ++ text e
    text (Eq l r) = "(" ++ text l ++ "==" ++ text r ++ ")"
instance DependsOn '[REnv] s => REnv (Boolean s) where
    renv (LitB b) = pure b
    renv (And l r) = (&&) <$> renv l <*> renv r
    renv (Or l r) = (||) <$> renv l <*> renv r
    renv (Cond i t e) = renv i >>= \b->if b then renv t else renv e
    renv (Eq l r) = (==) <$> renv l <*> renv r
instance DependsOn '[N] s => N (Boolean s) where
    nf (LitB b) = hashcons (nLitB b)
    nf (And l r) = nAnd <$> nf l <*> nf r >>= hashcons
    nf (Or l r) = nOr <$> nf l <*> nf r >>= hashcons
    -- We probably don't want sharing in the conditional branches
    nf (Cond i t e) = nCond <$> nf i <*> nf t <*> nf e >>= hashcons
    nf (Eq l r) = nEq <$> nf l <*> nf r >>= hashcons
instance DependsOn '[Size] s => Size (Boolean s) where
    size (LitB _) = 1
    size (And l r) = size l + size r + 1
    size (Or l r) = size l + size r + 1
    size (Cond i t e) = size i + size t + size e + 1
    size (Eq l r) = size l + size r + 1
instance DependsOn '[S] s => S (Boolean s) where
    s (LitB b) = pure (show b)
    s (And l r) = (\e1 e2->"(" ++ e1 ++ "&&" ++ e2 ++ ")") <$> s l <*> s r
    s (Or l r) = (\e1 e2->"(" ++ e1 ++ "||" ++ e2 ++ ")") <$> s l <*> s r
    s (Cond i t e) = (\e1 e2 e3->"if " ++ e1 ++ " then " ++ e2 ++ " else " ++ e3) <$> s i <*> s t <*> s e
    s (Eq l r) = (\e1 e2->"(" ++ e1 ++ "==" ++ e2 ++ ")") <$> s l <*> s r
