module ExprGADT where

import Data.Classy

main :: IO ()
main = do
    putStrLn "e0"
    putStrLn $ "text      : " ++ text e0
    putStrLn $ "eval      : " ++ show (eval e0)
    putStrLn $ "eval . opt: " ++ show (eval (opt e0))
    putStrLn $ "text . opt: " ++ text (opt e0)
    putStrLn "e1"
    putStrLn $ "text      : " ++ text e1
    putStrLn $ "eval      : " ++ show (eval e1)
    putStrLn $ "eval . opt: " ++ show (eval (opt e1))
    putStrLn $ "text . opt: " ++ text (opt e1)
    putStrLn "e2"
    putStrLn $ "text      : " ++ text e2
    putStrLn $ "eval      : " ++ show (eval e2)
    putStrLn $ "eval . opt: " ++ show (eval (opt e2))
    putStrLn $ "text . opt: " ++ text (opt e2)
    putStrLn "e2 (overloaded)"
    putStrLn $ "text      : " ++ text (e2' :: Expr '[Text] Int)
    putStrLn $ "eval      : " ++ show (eval (e2' :: Expr '[Eval] Int))
    putStrLn $ "eval . opt: " ++ show (eval (opt (e2' :: Expr '[Eval,Opt] Int)))
    putStrLn $ "text . opt: " ++ text (opt (e2' :: Expr '[Text,Opt] Int))
    putStrLn "e3 (overloaded) Int"
    putStrLn $ "text      : " ++ text (e3 :: Expr '[Text] Int)
    putStrLn $ "eval      : " ++ show (eval (e3 :: Expr '[Eval] Int))
    putStrLn $ "eval . opt: " ++ show (eval (opt (e3 :: Expr '[Eval,Opt] Int)))
    putStrLn $ "text . opt: " ++ text (opt (e3 :: Expr '[Text,Opt] Int))
    putStrLn "e3 (overloaded) Real"
    putStrLn $ "text      : " ++ text (e3 :: Expr '[Text] Double)
    putStrLn $ "eval      : " ++ show (eval (e3 :: Expr '[Eval] Double))
    putStrLn $ "eval . opt: " ++ show (eval (opt (e3 :: Expr '[Eval,Opt] Double)))
    putStrLn $ "text . opt: " ++ text (opt (e3 :: Expr '[Text,Opt] Double))

e0 :: Expr '[Eval,Text,Opt] Int
e0 = Lit 42

e0' :: (Show a, Eq a, Num a, Typeable c) => Expr c a
e0' = Lit 42

e1 :: Expr '[Eval,Text,Opt] Int
e1 = e0 `Add` Lit 0 `Add` Lit 3

e1' :: (Eq a, Show a, Num a, Typeable c) => Expr c a
e1' = e0' `Add` Lit 0 `Add` Lit 3

-- This is what you need to do by hand if you want to construct the record
--e2' :: Expr '[Eval,Text]
--e2' = Ext
--    (Cons (Cons Nil) :: Record (Subt '[Eval, Text]) '[Eval, Text])
--    (Lit 43 `Subt` Lit 1)

-- But it can be automated using smart constructors and a createRecord class
e2 :: Expr '[Eval,Text,Opt] Int
e2 = e1 `subt` Lit 1

-- Or it can be overloaded (but this requires you to list the views)
e2' :: (Show a, Eq a, Num a, Typeable a, UsingExt '[Subt] c) => Expr c a
e2' = e1' `subt` Lit 1

-- Or it can be overloaded (but this requires you to list the views)
e3 :: (Show a, Eq a, Num a, Typeable a, UsingExt '[Neg,Subt] c) => Expr c a
e3 = neg (neg e2')

subt :: (Num a, UsingExt '[Subt] c) => Expr c a -> Expr c a -> Expr c a
subt l r = Ext createRecord (l `Subt` r)

-- Main datat type, similar to the original classy deep embedding
data Expr c a where
    Lit :: (Eq a, Show a) => a -> Expr c a
    Add :: Num a => Expr c a -> Expr c a -> Expr c a
    Ext :: Typeable x => Record x c -> x a -> Expr c a

class Eval v where
    eval :: v a -> a

---- The instance is simplified a lot, only the `In` constraint is now added
instance DependsOn '[Eval] s => Eval (Expr s) where
    eval (Lit i) = i
    eval (Add l r) = eval l + eval r
    eval (Ext r (x :: x a)) = withDict (project r :: Dict (Eval x)) eval x

class Text v where
    text :: v a -> String

instance DependsOn '[Text] s => Text (Expr s) where
    text (Lit i) = show i
    text (Add l r) = "(" ++ text l ++ "+" ++ text r ++ ")"
    text (Ext r (x :: x a)) = withDict (project r :: Dict (Text x)) text x

data Subt c a where
    Subt :: Num a => Expr c a -> Expr c a -> Subt c a
    Subtloop :: Expr c a -> Subt c a

instance DependsOn '[Eval] s => Eval (Subt s) where
    eval (Subt l r) = eval l - eval r
    eval (Subtloop e) = eval e
instance DependsOn '[Text] s => Text (Subt s) where
    text (Subt l r) = "(" ++ text l ++ "-" ++ text r ++ ")"
    text (Subtloop e) = text e

class Opt v where
    opt :: v a -> v a

instance DependsOn '[Opt] s => Opt (Expr s) where
    opt (Lit i) = Lit i
    opt (Add l r) = case (opt l, opt r) of
        (Lit 0, r') -> r'
        (l', Lit 0) -> l'
        (l', r') -> Add l' r'
    opt (Ext r (x :: x a)) = Ext r (withDict (project r :: Dict (Opt x)) opt x)

instance DependsOn '[Opt] s => Opt (Subt s) where
    opt (Subtloop e) = Subtloop e
    opt (Subt l r) = case (opt l, opt r) of
        (l', Lit 0) -> Subtloop l'
        (l', r') -> Subt l' r'

data Neg c a where
    Neg :: (Num a, Typeable a) => Expr c a -> Neg c a
    Negloop :: Expr c a -> Neg c a

neg :: (Typeable a, Num a, UsingExt '[Neg] c) => Expr c a -> Expr c a
neg l = Ext createRecord (Neg l)

instance DependsOn '[Eval] s => Eval (Neg s) where
    eval (Neg x) = negate (eval x)
    eval (Negloop x) = eval x
instance DependsOn '[Text] s => Text (Neg s) where
    text (Neg x) = "(!" ++ text x ++ ")"
    text (Negloop x) = text x

instance (DependsOn '[Opt] s) => Opt (Neg s) where
    opt (Negloop e) = Negloop (opt e)
    opt (Neg e) = case opt e of
        (Ext _r (mExt -> Just (Neg x'))) -> Negloop x'
        e' -> Neg e'
