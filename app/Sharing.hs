{-# LANGUAGE StandaloneDeriving #-}
module Sharing where

-- Modelled after: O. Kiselyov, ‘Implementing Explicit and Finding Implicit Sharing in Embedded DSLs’, Electronic Proceedings in Theoretical Computer Science, vol. 66, pp. 210–225, Sep. 2011, doi: 10.4204/eptcs.66.11.

import Data.Classy
import Data.Maybe
import Data.Kind
import Data.Bits
import Control.Monad.Reader
import Control.Monad.State
import qualified Data.Map as DM

main :: IO ()
main = do
    putStrLn $ "expa: " ++ text expa
    putStrLn $ "expb: " ++ text expb
    putStrLn $ "expmul4: " ++ text (expmul4 :: Exp '[Text])
    putStrLn $ "sklansky: " ++ show (map text (sklansky Add (map (Variable . show) [1 :: Int ..4]) :: [Exp '[Text]]))
    putStrLn $ "testval4: " ++ show testval4
    putStrLn $ "n expmul4: " ++ show (runN (expmul4 :: Exp '[N]))
    putStrLn $ "n expmul8: " ++ show (runN (expmul8 :: Exp '[N]))
    putStrLn $ "testSklansky 4: " ++ show (testSklansky 4)
    putStrLn $ "size expmul4: " ++ show (size (expmul4 :: Exp '[Size]))
    putStrLn $ "size expmul4': " ++ show (size (expmul4' :: Exp '[Size]))
    putStrLn $ "valmul4: " ++ show (renv (expmul4 :: Exp '[REnv]) [("i1", 5)])
    putStrLn $ "valmul4': " ++ show (renv (expmul4' :: Exp '[REnv]) [("i1", 5)])
    putStrLn $ "valmul4'': " ++ show (renv (expmul4'' :: Exp '[REnv]) [("i1", 5)])
    putStrLn $ "show expmul4: " ++ s (expmul4 :: Exp '[S]) 0
    putStrLn $ "show expmul4: " ++ s (expmul4 :: Exp '[S]) 0
    putStrLn $ "size expmul4: " ++ show (size (expmul4 :: Exp '[Size]))
    putStrLn $ "size expmul4': " ++ show (size (expmul4'' :: Exp '[Size]))
    putStrLn $ "size mul 10 i: " ++ show (size (mul 12 (Variable "i") :: Exp '[Size]))
    putStrLn $ "size mul' 10 i: " ++ show (size (mul' 12 (Variable "i") :: Exp '[Size]))
    putStrLn "== without sharing =="
    putStrLn $ "n=19 (mul n (variable \"i\")): " ++ show (runN (mul (2 `shiftL` 19) (Variable "i") :: Exp '[N]))
    putStrLn $ "n=20 (mul n (variable \"i\")): " ++ show (runN (mul (2 `shiftL` 20) (Variable "i") :: Exp '[N]))
    putStrLn $ "n=21 (mul n (variable \"i\")): " ++ show (runN (mul (2 `shiftL` 21) (Variable "i") :: Exp '[N]))
    putStrLn "== with sharing =="
    putStrLn $ "n=19 (mul' n (variable \"i\")): " ++ show (runN (mul' (2 `shiftL` 19) (Variable "i") :: Exp '[N]))
    putStrLn $ "n=20 (mul' n (variable \"i\")): " ++ show (runN (mul' (2 `shiftL` 20) (Variable "i") :: Exp '[N]))
    putStrLn $ "n=21 (mul' n (variable \"i\")): " ++ show (runN (mul' (2 `shiftL` 21) (Variable "i") :: Exp '[N]))
    putStrLn $ "n=31 (mul' n (variable \"i\")): " ++ show (runN (mul' (2 `shiftL` 31) (Variable "i") :: Exp '[N]))

data Exp c
    =           Constant Int
    |           Variable String
    |           Add (Exp c) (Exp c)
    | forall x. Ext (Record x c) x

expa,expb :: Exp '[Text]
expa = Add (Constant 10) (Variable "i1")
expb = Add expa (Variable "i2")

mul :: Int -> Exp c -> Exp c
mul 0 _ = Constant 0
mul 1 x = x
mul n x | even n = mul (n `div` 2) (Add x x)
mul n x = Add x (mul (n-1) x)

expmul4,expmul8 :: Exp c
expmul4 = mul 4 (Variable "i1")
expmul8 = mul 8 (Variable "i1")

expmul4' :: Exp c
expmul4' = let x = Variable "i1"
          in let y = Add x x in Add y y

class Text v where
    text :: v -> String

instance DependsOn '[Text] s => Text (Exp s) where
    text (Constant i) = show i
    text (Add l r) = "(" ++ text l ++ "+" ++ text r ++ ")"
    text (Variable v) = v
    text (Ext r (e :: x)) = withDict (project r :: Dict (Text x)) text e

sklansky :: (a -> a -> a) -> [a] -> [a]
sklansky _ [] = []
sklansky _ [x] = [x]
sklansky f xs = left' ++ [f (last left') r | r <- right']
  where (left, right) = splitAt (length xs `div` 2) xs
        left' = sklansky f left
        right' = sklansky f right

class REnv v where
    renv :: v -> [(String, Int)] -> Int

instance DependsOn '[REnv] s => REnv (Exp s) where
    renv (Constant x) = pure x
    renv (Variable x) = asks (fromMaybe (error $ "no var: " ++ x) . lookup x)
    renv (Add l r) = (+) <$> renv l <*> renv r
    renv (Ext r (e :: x)) = withDict (project r :: Dict (REnv x)) renv e

testval4 :: Int
testval4 = renv (expmul4 :: Exp '[REnv]) [("i1",5)]

type NodeId = Int
data Node
    = NConst Int
    | NVar String
    | NAdd NodeId NodeId
    | forall x.(Eq x, Ord x, Show x) => NExt x
instance Eq Node where l == r = show l == show r
instance Ord Node where compare l r = compare (show l) (show r)
deriving instance Show Node

data BiMap a = BM {nextIdx :: Int, mapto :: DM.Map a Int, mapfro :: DM.Map Int a} deriving Show
newtype DAG = DAG (BiMap Node) deriving Show

lookupKey :: Ord a => a -> BiMap a -> Maybe Int
lookupKey a = DM.lookup a . mapto

lookupVal :: Int -> BiMap a -> Maybe a
lookupVal a = DM.lookup a . mapfro

empty :: BiMap a
empty = BM 0 DM.empty DM.empty

insert :: Ord a => a -> BiMap a -> (Int, BiMap a)
insert e (BM i mt mf) = (i, BM (i+1) (DM.insert e i mt) (DM.insert i e mf))

class N v where
    nf :: v -> State DAG NodeId

runN :: N v => v -> (NodeId, DAG)
runN v = runState (nf v) (DAG empty)

instance DependsOn '[N] s => N (Exp s) where
    nf (Constant x) = hashcons (NConst x)
    nf (Variable x) = hashcons (NVar x)
    nf (Add l r) = NAdd <$> nf l <*> nf r >>= hashcons
    nf (Ext r (e :: x)) = withDict (project r :: Dict (N x)) nf e

hashcons :: Node -> State DAG NodeId
hashcons e = do
    DAG m <- get
    case lookupKey e m of
        Nothing -> let (k, m') = insert e m
                   in  put (DAG m') >> pure k
        Just k -> pure k

testSklansky :: Int -> ([NodeId], DAG)
testSklansky n = runState sk (DAG empty)
  where sk = mapM nf (sklansky Add xs :: [Exp '[N]])
        xs = map (Variable . show) [1..n]

class Size v where
    size :: v -> Int
instance DependsOn '[Size] s => Size (Exp s) where
    size (Constant _) = 1
    size (Variable _) = 1
    size (Add x y) = size x + size y + 1
    size (Ext r (e :: x)) = withDict (project r :: Dict (Size x)) size e

data Let c = Let (Exp c) (Exp c -> Exp c)

let_ :: UsingExt '[Let] c => Exp c -> (Exp c -> Exp c) -> Exp c
let_ d b = Ext createRecord (Let d b)

instance DependsOn '[REnv] s => REnv (Let s) where
    renv (Let x f) = renv (f x)

instance DependsOn '[Size] s => Size (Let s) where
    size (Let x f) = size x + size (f (Variable ""))

expmul4'' :: UsingExt '[Let] c => Exp c
expmul4'' =
    let_ (Variable "i1") (\x->
    let_ (Add x x) (\y->
    Add y y))

class S v where
    s :: v -> Int -> String

instance DependsOn '[S] s => S (Exp s) where
    s (Constant i) = pure (show i)
    s (Variable x) = pure x
    s (Add l r) = (\e1 e2->"(" ++ e1 ++ "+" ++ e2 ++ ")") <$> s l <*> s r
    s (Ext r (e :: x)) = withDict (project r :: Dict (S x)) s e

instance DependsOn '[S] s => S (Let s) where
    s (Let def x) = do
        ni <- asks (("v"++) . show)
        pdef <- s def
        pbody <- local (+1) (s (x (Variable ni)))
        pure ("let " ++ show ni ++ " = " ++ pdef ++ " in " ++ pbody)

instance (UsingExt '[NodeIdTag] s, DependsOn '[N] s) => N (Let s) where
    nf (Let e f) = nf e >>= nf . f . Ext (createRecord :: Record (NodeIdTag s) s) . NodeIdTag

-- Auxilliary type so that we can inject NodeId's
newtype NodeIdTag (c :: [Type -> Constraint]) = NodeIdTag NodeId
instance N (NodeIdTag c) where
    nf (NodeIdTag i) = pure i

mul' :: UsingExt '[Let] c => Int -> Exp c -> Exp c
mul' 0 _ = Constant 0
mul' 1 x = x
mul' n x
    | even n    = let_ x (\x'->mul' (n `div` 2) (Add x' x'))
    | otherwise = let_ x (\x'->Add x' (mul' (n-1) x'))

-- Add subtraction
data Subt c = Subt (Exp c) (Exp c)
data NSubt = NSubt NodeId NodeId deriving (Eq, Ord, Show)
nSubt :: NodeId -> NodeId -> Node
nSubt l r = NExt (NSubt l r)
instance DependsOn '[Text] s => Text (Subt s) where
    text (Subt l r) = "(" ++ text l ++ "-" ++ text r ++ ")"
instance DependsOn '[REnv] s => REnv (Subt s) where
    renv (Subt l r) = (-) <$> renv l <*> renv r
instance DependsOn '[N] s => N (Subt s) where
    nf (Subt l r) = nSubt <$> nf l <*> nf r >>= hashcons
instance DependsOn '[Size] s => Size (Subt s) where
    size (Subt l r) = size l + size r + 1
instance DependsOn '[S] s => S (Subt s) where
    s (Subt l r) = (\e1 e2->"(" ++ e1 ++ "-" ++ e2 ++ ")") <$> s l <*> s r
