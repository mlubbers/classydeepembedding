module Data.Classy (
    -- * Dependencies
      module Data.Constraint
    , module Data.Dynamic
    -- * Data types
    , Record
    -- * Type classes
    , In (..)
    , CreateRecord (..)
    , UsingExt
    , DependsOn
    , mExt
    ) where

import Data.Constraint
import Data.Dynamic

-- | Structure to store all the class constraints (i.e. views)
-- This is always automatically created by using 'CreateRecord'
data Record (dt :: k) (clist :: [k -> Constraint]) where
    Nil  :: Record dt '[]
    Cons :: c dt => Record dt cs -> Record dt (c ': cs)

-- | Extract class constraints from a 'Record' structure
class c `In` cs where
    project :: Record dt cs -> Dict (c dt)
instance {-# OVERLAPPING #-} c `In` (c ': cs) where
    project (Cons _) = Dict
instance {-# OVERLAPPING #-} c `In` cs => c `In` (b ': cs) where
    project (Cons xs) = project xs

-- | Create records automatically, requires instances for every data type
class CreateRecord dt c where
    createRecord :: Record dt c
instance CreateRecord d '[] where
    createRecord = Nil
instance (c (d c0), CreateRecord (d c0) cs) => CreateRecord (d c0) (c ': cs) where
    createRecord = Cons createRecord

-- | Type family to succinctly bundle CreateRecord constraints
type family UsingExt cs c :: Constraint where
    UsingExt '[] c = Typeable c
    UsingExt (d ': cs) c = (CreateRecord (d c) c, UsingExt cs c)

-- | Type family to succinctly bundle `In` constraints
type family DependsOn cs c :: Constraint where
    DependsOn '[] c = Typeable c
    DependsOn (d ': cs) c = (d `In` c, DependsOn cs c)

-- | Match an extension (to be used as a safe(r) viewpattern
mExt :: (Typeable c, Typeable x0, Typeable x1) => x0 -> Maybe (x1 c)
mExt = fromDynamic . toDyn
